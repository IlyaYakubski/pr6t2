import java.util.Scanner;

public class Pr6t2 {
    public static void main(String[] args) {
        int x1, x2, x3, x4;
        int gcd = 0;
        x1 = input();
        x2 = input();
        x3 = input();
        x4 = input();
        gcd = gcd(x1, x2, x3, x4, gcd);
        System.out.println("Greatest common divisor is " + gcd);
    }

    public static int input() {
        int value;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter your value");
        while (!scan.hasNextInt()) {
            scan.nextLine();
        }
        value = scan.nextInt();
        return value;
    }
    public static int gcd(int x1, int x2, int x3, int x4, int gcd) {
        for (int i = 1; i <= x1 && i <= x2 && i <= x3 && i <= x4; i++) {
            if (x1 % i == 0 && x2 % i == 0 && x3 % i == 0 && x4 % i == 0) {
                gcd = i;
            }
        }
        return gcd;
    }
}
